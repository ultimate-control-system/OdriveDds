//
// Created by Teodor on 20.10.2022.
//

#ifndef ODRIVEDDS_ODRIVECANVERTER_H
#define ODRIVEDDS_ODRIVECANVERTER_H

#include <cstdint>
#include <cstdio>

#include "OdriveDdsUtilities.h"
#include "DdsSubscriber.hpp"
#include "DdsPublisher.hpp"
#include "CanFrame/CanFramePubSubTypes.h"

using namespace eprosima::fastdds::dds;

constexpr const char *OdriveCommandIdText(EOdriveCommandId id);

constexpr const char *OdriveAxisStateText(EOdriveAxisState state);

std::ostream &operator<<(std::ostream &ost, EOdriveCommandId id);

std::ostream &operator<<(std::ostream &ost, EOdriveAxisState state);

std::ostream &operator<<(std::ostream &ost, EMotorError error);

struct OdriveMessageId {
    // Underlying datatype
    using IdType = uint32_t;

    explicit OdriveMessageId(IdType messageId = 0) : commandId(EOdriveCommandId::OdriveHeartbeat), axisId(0),
                                                     rtrFlag(0),
                                                     errorFlag(0), effFlag(0) { *this = messageId; }

    OdriveMessageId(
            EOdriveCommandId commandId,
            IdType axisId,
            bool rtrFlag = false,
            bool errorFlag = false,
            bool effFlag = false
    )
            : commandId(commandId), axisId(axisId), rtrFlag(rtrFlag), errorFlag(errorFlag), effFlag(effFlag) {}

    OdriveMessageId &operator=(IdType other) {
        *reinterpret_cast<IdType *>(this) = other;
        return *this;
    }

    explicit operator IdType() const { return *reinterpret_cast<const IdType *>(this); }

    friend std::ostream &operator<<(std::ostream &ost, const OdriveMessageId &msg);

    EOdriveCommandId commandId: 5;
    IdType axisId: 24;
    IdType errorFlag: 1;
    IdType rtrFlag: 1;
    IdType effFlag: 1;
};

class OdriveAxis {
    friend OdriveCanVerter;

CALLBACK_DEFINITIONS_TEMPLATE(DataAvailable, void, OdriveMessageId)

public:
    using IdType = OdriveMessageId::IdType;

    OdriveAxis(OdriveCanVerter *parent, IdType axisId);

    [[nodiscard]] IdType axisId() const { return _axisId; }
    [[nodiscard]] EOdriveAxisState state() const { return _state; }
    [[nodiscard]] uint32_t error() const { return _error; }
    [[nodiscard]] uint8_t controllerStatus() const { return _controllerStatus; }
    [[nodiscard]] uint8_t motorStatus() const { return _motorStatus; }
    [[nodiscard]] uint8_t encoderStatus() const { return _encoderStatus; }
    [[nodiscard]] float vbusVoltage() const { return _vbusVoltage; }
    [[nodiscard]] int32_t shadowCount() const { return _shadowCount; }
    [[nodiscard]] int32_t countCpr() const { return _countCpr; }
    [[nodiscard]] float position() const { return _position; }
    [[nodiscard]] float velocity() const { return _velocity; }
    [[nodiscard]] float iqSetpoint() const { return _iqSetpoint; }
    [[nodiscard]] float iqMeasured() const { return _iqMeasured; }
    [[nodiscard]] EMotorError motorError() const { return _motorError; }

    // Can frames
    [[maybe_unused]] void oDriveEstop();
    [[maybe_unused]] void getMotorError();
    [[maybe_unused]] void getEncoderError();
    [[maybe_unused]] void getSensorlessError();
    [[maybe_unused]] bool setAxisNodeId(IdType newId);
    [[maybe_unused]] void setRequestState(EOdriveAxisState state);
    [[maybe_unused]] void fetchEncoderEstimates();
    [[maybe_unused]] void fetchEncoderCount();
    [[maybe_unused]] void setControllerModes(EOdriveControlMode controlMode, EOdriveInputMode inputMode);
    [[maybe_unused]] void setInputPos(OdriveInputPosMessage inputPosMessage);
    [[maybe_unused]] void setInputVel(OdriveInputVelMessage inputVelMessage);
    [[maybe_unused]] void setInputTorque(float inputTorque);
    [[maybe_unused]] void setLimits(OdriveLimitMessage limitMessage);
    [[maybe_unused]] void startAntiCogging();
    [[maybe_unused]] void setTrajVelLimit(OdriveTrajVelLimitMessage limitMessage);
    [[maybe_unused]] void setTrajAccelLimits(OdriveTrajAccelLimitsMessage limitMessage);
    [[maybe_unused]] void setTrajInertia(OdriveTrajInertiaMessage inertiaMessage);
    [[maybe_unused]] void fetchIq();
    [[maybe_unused]] void fetchSensorlessEstimates();
    [[maybe_unused]] void rebootOdrive();
    [[maybe_unused]] void fetchVbusVoltage();
    [[maybe_unused]] void clearErrors();
    [[maybe_unused]] void setLinearCount(OdriveLinearCountMessage countMessage);
    [[maybe_unused]] void setPositionGain(OdrivePositionGainMessage gainMessage);
    [[maybe_unused]] void setVelocityGains(OdriveVelGainsMessage gainsMessage);

private:
    OdriveCanVerter *_parent;
    IdType _axisId;
    EOdriveAxisState _state;
    float _vbusVoltage;
    uint32_t _error;
    uint8_t _controllerStatus; // ToDo: Figure out the bitfield of this
    uint8_t _motorStatus{}; // ToDo: Figure out the bitfield of this
    uint8_t _encoderStatus{}; // ToDo: Figure out the bitfield of this
    int32_t _shadowCount{};
    int32_t _countCpr{};
    float _position{};
    float _velocity{};
    float _iqSetpoint{};
    float _iqMeasured{};
    EMotorError _motorError;


};

class OdriveCanVerter {
public:
    using IdType = OdriveMessageId::IdType;

    // ToDo: remember to remove this!
    [[maybe_unused]] void testTriggerOnDataAvailable(const CanFrame &msg) {
        _subscriber.testTriggerOnDataAvailable(msg);
    }

    OdriveCanVerter(const char *publisherTopicName, const char *subscriberTopicName,
                    DomainParticipant *participant);

    bool addAxis([[maybe_unused]] OdriveAxis *axis);

    [[maybe_unused]] void removeAxis(OdriveAxis *axis);

    OdriveAxis *findAxis(IdType axisId);

    void oDriveEstop(IdType axisId);

    void fetchMotorError(IdType axisId);

    [[maybe_unused]] void fetchEncoderError(IdType axisId);

    void fetchSensorlessError(IdType axisId);

    bool setAxisNodeId(OdriveAxis *axis, IdType newId);

    void setRequestState(IdType axisId, EOdriveAxisState state);

    void fetchEncoderEstimate(IdType axisId);

    void fetchEncoderCount(IdType axisId);

    void setControllerModes(IdType axisId, EOdriveInputMode inputMode, EOdriveControlMode controllerMode);

    void setInputPosition(IdType axisId, OdriveInputPosMessage inputPosMessage);

    void setInputVelocity(IdType axisId, OdriveInputVelMessage inputVelMessage);

    void setInputTorque(IdType axisId, float inputTorque);

    void setLimits(IdType axisId, OdriveLimitMessage limitMessage);

    [[maybe_unused]] void startAntiCogging(IdType axisId);

    //    StartAnticogging	=	0x010,
    void setTrajVelLimit(IdType axisId, OdriveTrajVelLimitMessage limitMessage);

    void setTrajAccelLimits(IdType axisId, OdriveTrajAccelLimitsMessage limitMessage);

    void setTrajInertia(IdType axisId, OdriveTrajInertiaMessage inertiaMessage);

    void fetchIq(IdType axisId);

    void fetchSensorlessEstimates(IdType axisId);

    void rebootOdrive(IdType axisId);

    void fetchVbusVoltage(IdType axisId);

    void clearErrors(IdType axisId);

    void setLinearCount(IdType axisId, OdriveLinearCountMessage countMessage);

    void setPositionGain(IdType axisId, OdrivePositionGainMessage gainMessage);

    void setVelocityGains(IdType axisId, OdriveVelGainsMessage gainsMessage);

private:

    void _onDataAvailable(const CanFrame &msg, const SampleInfo &info);

    std::vector<OdriveAxis *> _axes;

    DdsPublisher<CanFramePubSubType> _publisher;
    DdsSubscriber<CanFramePubSubType> _subscriber;

    struct {
        OdriveAxis *axis;
        IdType newId;
    } _pendingIdChange;

    //OdriveHeartbeat	        =	0x001,
    static void UnpackHeartbeat(OdriveAxis *axis, const CanFrame &frame);

    //GetMotorError	            =	0x003,
    static void UnpackMotorError(OdriveAxis *axis, const CanFrame &frame);

    //GetEncoderError	        =	0x004,
    static void UnpackEncoderError(OdriveAxis *axis, const CanFrame &frame);

    //GetSensorlessError	    =	0x005,
    static void UnpackSensorlessError(OdriveAxis *axis, const CanFrame &frame);

    //GetEncoderEstimates	    =	0x009,
    static void UnpackEncoderEstimates(OdriveAxis *axis, const CanFrame &frame);

    //GetEncoderCount	        =	0x00A,
    static void UnpackEncoderCount(OdriveAxis *axis, const CanFrame &frame);

    //GetIq	                    =	0x014,
    static void UnpackIq(OdriveAxis *axis, const CanFrame &frame);

    //GetSensorlessEstimates	=	0x015,
    static void UnpackSensorlessEstimates(OdriveAxis *axis, const CanFrame &frame);

    //GetVbusVoltage	        =	0x017,
    static void UnpackVbusVoltage(OdriveAxis *axis, const CanFrame &frame);

};

#endif //ODRIVEDDS_ODRIVECANVERTER_H
