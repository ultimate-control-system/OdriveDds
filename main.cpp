#include <iostream>

#include "OdriveCanVerter.h"




int main()
{


    ScopedDomainParticipant participant("Participant");

    OdriveCanVerter canVerter("CanTransmit", "CanReceive", participant.participant());

    OdriveAxis axis(&canVerter, 0);
    axis.bindOnDataAvailable([&axis](OdriveMessageId id){
        //std::cout << id << " from " << axis.axisId() << std::endl;
        switch (id.commandId)
        {
            case EOdriveCommandId::OdriveHeartbeat:
                break;
                std::cout << "*** Heartbeat message ***\n"
                    << "State:             " << axis.state()
                    << "\nError:             " << axis.error()
                    << "\nController status: " << uint32_t(axis.controllerStatus())
                    << "\nEncoder status: " << uint32_t(axis.encoderStatus())
                    << "\nMotor status: " << uint32_t(axis.motorStatus())
                    << "\n"<<std::endl;
                break;

            case EOdriveCommandId::GetVbusVoltage:
                std::cout << "*** Voltage message ***\n"
                        << "Vbus voltage: " << axis.vbusVoltage()
                        << "\n"<<std::endl;
                break;
            case EOdriveCommandId::GetEncoderEstimates:
                std::cout << "*** Encoder estimate message ***\n"
                        << "Encoder position: " << axis.position()
                        << "\nEncoder velocity: " << axis.velocity()
                        << "\n"<<std::endl;
                break;

            case EOdriveCommandId::GetEncoderCount:
                std::cout << "*** Encoder count message ***\n"
                        << "Encoder shadow count: " << axis.shadowCount()
                        << "\nEncoder count cpr: " << axis.countCpr()
                        << "\n"<<std::endl;
                break;

            case EOdriveCommandId::GetIq:
                std::cout << "*** Iq message ***\n"
                        << "Iq setpoint: " << axis.iqSetpoint()
                        << "\nIq measured: " << axis.iqMeasured()
                        << "\n"<<std::endl;
                break;

            case EOdriveCommandId::GetSensorlessEstimates:
                std::cout << "*** Sensorless estimate message ***\n"
                        << "Estimated position: " << axis.position()
                        << "\nEstimated velocity: " << axis.velocity()
                        << "\n"<<std::endl;
                break;

            case EOdriveCommandId::GetMotorError:
                std::cout << "*** Motor error message ***\n"
                        << "Estimated position: " << axis.motorError()
                        << "\n"<<std::endl;
                break;

            default: break;
        }
    });


    using namespace std::chrono_literals;

    std::this_thread::sleep_for(500ms);

    int count = 10;
    axis.clearErrors();
    std::this_thread::sleep_for(500ms);
    axis.fetchVbusVoltage();
    std::this_thread::sleep_for(500ms);
    axis.fetchEncoderEstimates();
    std::this_thread::sleep_for(500ms);
    axis.fetchEncoderEstimates();
    std::this_thread::sleep_for(500ms);

    axis.fetchEncoderCount();
    std::this_thread::sleep_for(500ms);
    axis.fetchIq();
    axis.setRequestState(EOdriveAxisState::Idle);
    std::this_thread::sleep_for(500ms);

    EOdriveInputMode inputMode;
    inputMode = EOdriveInputMode::VelRamp;
    EOdriveControlMode controlMode;
    controlMode = EOdriveControlMode::ControlModeVelocityControl;
    axis.setControllerModes(controlMode, inputMode);

    OdriveInputVelMessage msg;
    msg.inputVelocity = -1;
    axis.setInputVel(msg);
    std::this_thread::sleep_for(500ms);
    controlMode = EOdriveControlMode::ControlModePositionControl;


    // Send a heartbeat message
    //CanFrame frame;
    //frame.can_id(uint32_t(OdriveMessageId(EOdriveCommandId::OdriveHeartbeat, axis.axisId())));
    //auto msg = reinterpret_cast<OdriveHearbeatMessage*>(frame.data().data());
    //msg->state = uint8_t(EOdriveAxisState::FullCalibrationSequence);
    //msg->controllerStatus = 12;
    //msg->error = 55;

    //canVerter.testTriggerOnDataAvailable(frame);

    // Attempt to change id
    //uint32_t newId = 55;
    //axis.setAxisNodeId(newId);

    // Send a vbus voltage message with the new id
    //frame.can_id(uint32_t(OdriveMessageId(EOdriveCommandId::GetVbusVoltage, newId, true)));
    //auto msg2 = reinterpret_cast<float*>(frame.data().data());
    //*msg2 = 123.564f;

    //canVerter.testTriggerOnDataAvailable(frame);


    return 0;
}
