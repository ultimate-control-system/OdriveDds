//
// Created by Teodor on 20.10.2022.
//

#include "OdriveCanVerter.h"

//-------------------------------------------------------------------
//---------------------------- Static -------------------------------

constexpr const char *OdriveCommandIdText(EOdriveCommandId id) {
//    if (id == EOdriveCommandId::CanOpenHeartbeatMessage)
//        return "CanOpen Heartbeat Message";
//    else
    if (id < EOdriveCommandId::EndOfList)
        return OdriveCommandIdTextList[uint32_t(id)];

    return "Unknown";
}

constexpr const char *OdriveAxisStateText(EOdriveAxisState state) {
    if (state >= EOdriveAxisState::EndOfList)
        return "Unknown";

    return OdriveAxisStateTextList[uint32_t(state)];
}
constexpr const char *MotorErrorText(EMotorError error) {
    if (error >= EMotorError::EndOfList)
        return "Unknown";

    return "Motor error comming here, but for now its code" + uint64_t(error);
}
std::ostream &operator<<(std::ostream &ost, EOdriveCommandId id) {
    return (ost << OdriveCommandIdText(id));
}

std::ostream &operator<<(std::ostream &ost, EOdriveAxisState state) {
    return (ost << OdriveAxisStateText(state));
}

std::ostream &operator<<(std::ostream &ost, EMotorError error) {
    return (ost << MotorErrorText(error));
}

std::ostream &operator<<(std::ostream &ost, const OdriveMessageId &msg) {
    return (ost
            << "[ " << msg.axisId
            << ", " << msg.commandId
            << (msg.rtrFlag ? ", rtr" : "")
            << (msg.errorFlag ? ", err" : "")
            << (msg.effFlag ? ", eff" : "")
            << "]"
    );
}

//-------------------------------------------------------------------
//------------------------- OdriveAxis ------------------------------
OdriveAxis::OdriveAxis(OdriveCanVerter *parent, OdriveAxis::IdType axisId)
        : _parent(parent), _axisId(axisId), _state(EOdriveAxisState(-1)), _vbusVoltage(0.f), _error(0),
          _controllerStatus(0) {
    _parent->addAxis(this);
}

[[maybe_unused]] void OdriveAxis::oDriveEstop() {
    _parent->oDriveEstop(_axisId);
}
[[maybe_unused]] void OdriveAxis::getMotorError() {
    _parent->fetchMotorError(_axisId);
}

[[maybe_unused]] void OdriveAxis::getEncoderError() {
    _parent->fetchEncoderError(_axisId);
}
[[maybe_unused]] void OdriveAxis::getSensorlessError() {
    _parent->fetchSensorlessError(_axisId);
}
[[maybe_unused]] void OdriveAxis::setRequestState(EOdriveAxisState state) {
    _parent->setRequestState(_axisId, state);
}

[[maybe_unused]] bool OdriveAxis::setAxisNodeId(OdriveAxis::IdType newId) {
    return _parent->setAxisNodeId(this, newId);
}

[[maybe_unused]] void OdriveAxis::fetchEncoderEstimates() {
    _parent->fetchEncoderEstimate(_axisId);
}

[[maybe_unused]] void OdriveAxis::fetchEncoderCount() {
    _parent->fetchEncoderCount(_axisId);
}

[[maybe_unused]] void OdriveAxis::setControllerModes(EOdriveControlMode controlMode, EOdriveInputMode inputMode) {
    _parent->setControllerModes(_axisId, inputMode, controlMode);
}

[[maybe_unused]] void OdriveAxis::setInputPos(OdriveInputPosMessage inputPosMessage) {
    _parent->setInputPosition(_axisId, inputPosMessage);
}

[[maybe_unused]] void OdriveAxis::setInputVel(OdriveInputVelMessage inputVelMessage) {
    _parent->setInputVelocity(_axisId, inputVelMessage);
}

[[maybe_unused]] void OdriveAxis::setInputTorque(float inputTorque) {
    _parent->setInputTorque(_axisId, inputTorque);
}

[[maybe_unused]] void OdriveAxis::setLimits(OdriveLimitMessage limitMessage) {
    _parent->setLimits(_axisId, limitMessage);
}

[[maybe_unused]]void OdriveAxis::startAntiCogging() {
    _parent->startAntiCogging(_axisId);
}

[[maybe_unused]] void OdriveAxis::setTrajVelLimit(OdriveTrajVelLimitMessage limitMessage) {
    _parent->setTrajVelLimit(_axisId, limitMessage);
}

[[maybe_unused]] void OdriveAxis::setTrajAccelLimits(OdriveTrajAccelLimitsMessage limitMessage) {
    _parent->setTrajAccelLimits(_axisId, limitMessage);
}

[[maybe_unused]] void OdriveAxis::setTrajInertia(OdriveTrajInertiaMessage inertiaMessage) {
    _parent->setTrajInertia(_axisId, inertiaMessage);
}

void OdriveAxis::fetchIq() {
    _parent->fetchIq(_axisId);
}

[[maybe_unused]] void OdriveAxis::fetchSensorlessEstimates() {
    _parent->fetchSensorlessEstimates(_axisId);
}

[[maybe_unused]]void OdriveAxis::rebootOdrive() {
    _parent->rebootOdrive(_axisId);
}

[[maybe_unused]]void OdriveAxis::fetchVbusVoltage() {
    _parent->fetchVbusVoltage(_axisId);
}

[[maybe_unused]]void OdriveAxis::clearErrors() {
    _parent->clearErrors(_axisId);
}

[[maybe_unused]] void OdriveAxis::setLinearCount(OdriveLinearCountMessage countMessage) {
    _parent->setLinearCount(_axisId, countMessage);
}

[[maybe_unused]]void OdriveAxis::setPositionGain(OdrivePositionGainMessage gainMessage) {
    _parent->setPositionGain(_axisId, gainMessage);
}

[[maybe_unused]]void OdriveAxis::setVelocityGains(OdriveVelGainsMessage gainsMessage) {
    _parent->setVelocityGains(_axisId, gainsMessage);
}






//-------------------------------------------------------------------
//------------------------ OdriveCanVerter --------------------------

OdriveCanVerter::OdriveCanVerter(
        const char *publisherTopicName,
        const char *subscriberTopicName,
        DomainParticipant *participant
) : _pendingIdChange({nullptr, 0}) {
    _publisher.init(participant, publisherTopicName);
    _subscriber.init(participant, subscriberTopicName);

    _subscriber.bindOnDataAvailable(this, &OdriveCanVerter::_onDataAvailable);
}


bool OdriveCanVerter::setAxisNodeId(OdriveAxis *axis, OdriveCanVerter::IdType newId) {
    // Check if new id is already in use
    auto exists = findAxis(newId);
    if (exists)
        return true;

    // Save id and axis in order to update id on next received message from the new id
    _pendingIdChange.axis = axis;
    _pendingIdChange.newId = newId;

    // Create and send the message
    OdriveMessageId messageId(EOdriveCommandId::SetAxisNodeId, axis->_axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(4);

    auto data = reinterpret_cast<uint32_t *>(frame.data().data());
    *data = newId;

    _publisher.publish(frame);

    return false;
}

void OdriveCanVerter::fetchVbusVoltage(IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetVbusVoltage, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);

}

void OdriveCanVerter::fetchEncoderEstimate(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetEncoderEstimates, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

void OdriveCanVerter::fetchEncoderCount(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetEncoderCount, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

void OdriveCanVerter::setRequestState(IdType axisId, EOdriveAxisState state) {
    OdriveMessageId messageId(EOdriveCommandId::SetAxisRequestedState, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(4);
    auto data = reinterpret_cast<uint32_t *>(frame.data().data());
    *data = uint32_t(state);

    _publisher.publish(frame);
}

void OdriveCanVerter::setControllerModes(OdriveCanVerter::IdType axisId, EOdriveInputMode inputMode,
                                         EOdriveControlMode controllerMode) {
    OdriveMessageId messageId(EOdriveCommandId::SetControllerModes, axisId);
    OdriveControllerMessage msg;
    msg.controlMode = uint32_t(controllerMode);
    msg.inputMode = uint32_t(inputMode);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);
    auto data = reinterpret_cast<OdriveControllerMessage *>(frame.data().data());

    *data = msg;

    _publisher.publish(frame);
}

void OdriveCanVerter::setInputPosition(OdriveCanVerter::IdType axisId, OdriveInputPosMessage inputPosMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetInputPos, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);

    auto data = reinterpret_cast<OdriveInputPosMessage *>(frame.data().data());

    *data = inputPosMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setInputVelocity(OdriveCanVerter::IdType axisId, OdriveInputVelMessage inputVelMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetInputVel, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);

    auto data = reinterpret_cast<OdriveInputVelMessage *>(frame.data().data());

    *data = inputVelMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setInputTorque(OdriveCanVerter::IdType axisId, float inputTorque) {
    OdriveMessageId messageId(EOdriveCommandId::SetInputTorque, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(4);
    auto data = reinterpret_cast<float *>(frame.data().data());
    *data = inputTorque;

    _publisher.publish(frame);
}

void OdriveCanVerter::fetchIq(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetIq, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

void OdriveCanVerter::clearErrors(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::ClearErrors, axisId, false);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

bool OdriveCanVerter::addAxis(OdriveAxis *axis) {
    auto ax = findAxis(axis->axisId());
    if (ax)
        return true;

    _axes.emplace_back(axis);
    return false;
}

OdriveAxis *OdriveCanVerter::findAxis(OdriveCanVerter::IdType axisId) {
    for (auto axis: _axes) {
        if (axis->axisId() == axisId)
            return axis;
    }

    return nullptr;
}

[[maybe_unused]] [[maybe_unused]] void OdriveCanVerter::removeAxis(OdriveAxis *axis) {
    if (_axes.empty())
        return;

    auto end = _axes.end();
    auto ax = std::find(_axes.begin(), end, axis);

    if (ax != end)
        _axes.erase(ax);
}

void OdriveCanVerter::_onDataAvailable(const CanFrame &msg, const SampleInfo &info) {
    if (!info.valid_data)
        return;

    OdriveMessageId msgId(msg.can_id());

    auto axis = findAxis(msgId.axisId);
    if (!axis) {
        // Check for pending id change
        if (_pendingIdChange.axis && (_pendingIdChange.newId == msgId.axisId)) {
            axis = _pendingIdChange.axis;
            axis->_axisId = msgId.axisId;
            _pendingIdChange.axis = nullptr;
        } else
            return;
    }

    // Possible return messages
    //OdriveHeartbeat	        =	0x001,
    //GetMotorError	            =	0x003,
    //GetEncoderError	        =	0x004,
    //GetSensorlessError	    =	0x005,
    //GetEncoderEstimates	    =	0x009,
    //GetEncoderCount	        =	0x00A,
    //GetIq	                    =	0x014,
    //GetSensorlessEstimates	=	0x015,
    //GetVbusVoltage	        =	0x017,
    switch (msgId.commandId) {
        case EOdriveCommandId::OdriveHeartbeat:
            UnpackHeartbeat(axis, msg);
            break;
        case EOdriveCommandId::GetMotorError:
            UnpackMotorError(axis, msg);
            break;
        case EOdriveCommandId::GetEncoderError:
            UnpackEncoderError(axis, msg);
            break;
        case EOdriveCommandId::GetSensorlessError:
            UnpackSensorlessError(axis, msg);
            break;
        case EOdriveCommandId::GetEncoderEstimates:
            UnpackEncoderEstimates(axis, msg);
            break;
        case EOdriveCommandId::GetEncoderCount:
            UnpackEncoderCount(axis, msg);
            break;
        case EOdriveCommandId::GetIq:
            UnpackIq(axis, msg);
            break;
        case EOdriveCommandId::GetSensorlessEstimates:
            UnpackSensorlessEstimates(axis, msg);
            break;
        case EOdriveCommandId::GetVbusVoltage:
            UnpackVbusVoltage(axis, msg);
            break;
        default:
            break;
    }

    if (axis->_onDataAvailable)
        axis->_onDataAvailable(msgId);

}

void OdriveCanVerter::UnpackHeartbeat(OdriveAxis *axis, const CanFrame &frame) {
    OdriveHearbeatMessage msg(frame.data().data());

    axis->_state = msg.stateEnum();
    axis->_error = msg.error;
    axis->_controllerStatus = msg.controllerStatus;
    axis->_motorStatus = msg.motorStatus;
    axis->_encoderStatus = msg.encoderStatus;
}

void OdriveCanVerter::UnpackMotorError(OdriveAxis *axis, const CanFrame &frame) {

    axis->_motorError = EMotorError(*reinterpret_cast< uint64_t *>(*frame.data().data()));

}

void OdriveCanVerter::UnpackEncoderError(OdriveAxis *axis, const CanFrame &frame) {
    (void)axis;
}

void OdriveCanVerter::UnpackSensorlessError(OdriveAxis *axis, const CanFrame &frame) {
    OdriveEncoderEstimateMessage msg(frame.data().data());
    // The data frame is the same so guess the message would be the same as well
    axis->_position = msg.position;
    axis->_velocity = msg.velocity;
}

void OdriveCanVerter::UnpackEncoderEstimates(OdriveAxis *axis, const CanFrame &frame) {
    OdriveEncoderEstimateMessage msg(frame.data().data());
    axis->_position = msg.position;
    axis->_velocity = msg.velocity;
}

void OdriveCanVerter::UnpackEncoderCount(OdriveAxis *axis, const CanFrame &frame) {
    OdriveEncoderCountMessage msg(frame.data().data());
    axis->_shadowCount = msg.shadowCount;
    axis->_countCpr = msg.countCpr;
}

void OdriveCanVerter::UnpackIq(OdriveAxis *axis, const CanFrame &frame) {
    OdriveIqMessage msg(frame.data().data());
    axis->_iqMeasured = msg.iqMeasured;
    axis->_iqSetpoint = msg.iqSetpoint;
}

void OdriveCanVerter::UnpackSensorlessEstimates(OdriveAxis *axis, const CanFrame &frame) {
    (void)axis;
}

void OdriveCanVerter::UnpackVbusVoltage(OdriveAxis *axis, const CanFrame &frame) {
    axis->_vbusVoltage = *reinterpret_cast<const float *>(frame.data().data());
}

void OdriveCanVerter::fetchSensorlessEstimates(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetSensorlessEstimates, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

void OdriveCanVerter::setLinearCount(OdriveCanVerter::IdType axisId, OdriveLinearCountMessage countMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetLinearCount, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(4);

    auto data = reinterpret_cast<OdriveLinearCountMessage *>(frame.data().data());

    *data = countMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::rebootOdrive(IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::RebootOdrive, axisId, false);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

void OdriveCanVerter::setPositionGain(OdriveCanVerter::IdType axisId, OdrivePositionGainMessage gainMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetPositionGain, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(4);

    auto data = reinterpret_cast<OdrivePositionGainMessage *>(frame.data().data());

    *data = gainMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setVelocityGains(OdriveCanVerter::IdType axisId, OdriveVelGainsMessage gainsMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetVelGains, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);

    auto data = reinterpret_cast<OdriveVelGainsMessage *>(frame.data().data());

    *data = gainsMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setTrajAccelLimits(OdriveCanVerter::IdType axisId, OdriveTrajAccelLimitsMessage limitMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetTrajAccelLimits, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);

    auto data = reinterpret_cast<OdriveTrajAccelLimitsMessage *>(frame.data().data());

    *data = limitMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setTrajInertia(OdriveCanVerter::IdType axisId, OdriveTrajInertiaMessage inertiaMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetTrajInertia, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);

    auto data = reinterpret_cast<OdriveTrajInertiaMessage *>(frame.data().data());

    *data = inertiaMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setTrajVelLimit(OdriveCanVerter::IdType axisId, OdriveTrajVelLimitMessage limitMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetTrajVelLimit, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(4);

    auto data = reinterpret_cast<OdriveTrajVelLimitMessage *>(frame.data().data());

    *data = limitMessage;

    _publisher.publish(frame);
}

void OdriveCanVerter::setLimits(OdriveCanVerter::IdType axisId, OdriveLimitMessage limitMessage) {
    OdriveMessageId messageId(EOdriveCommandId::SetLimits, axisId);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(8);

    auto data = reinterpret_cast<OdriveLimitMessage *>(frame.data().data());

    *data = limitMessage;

    _publisher.publish(frame);
}

[[maybe_unused]] void OdriveCanVerter::oDriveEstop(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::OdriveEstop, axisId, false);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

[[maybe_unused]] void OdriveCanVerter::fetchMotorError(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetMotorError, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

[[maybe_unused]] void OdriveCanVerter::fetchEncoderError(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetEncoderError, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

[[maybe_unused]] void OdriveCanVerter::fetchSensorlessError(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::GetSensorlessError, axisId, true);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}

//! Anti cogging
//! https://hackaday.com/2016/02/23/anti-cogging-algorithm-brings-out-the-best-in-your-hobby-brushless-motors/
//! \param axisId
[[maybe_unused]] void OdriveCanVerter::startAntiCogging(OdriveCanVerter::IdType axisId) {
    OdriveMessageId messageId(EOdriveCommandId::StartAnticogging, axisId, false);

    CanFrame frame;
    frame.can_id(IdType(messageId));
    frame.len(0);

    _publisher.publish(frame);
}









